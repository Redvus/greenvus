;(function () {

    //Scrollify
    if($(window).width() > 768 || screen.width > 768) {
        //Scrollify. Пролистывание страниц с фиксацией и анимацией
        $.scrollify({
            section : "section",
            // interstitialSection : ".footer",
            easing: "easeOutExpo",
            scrollbars: false,
            // before:function(i,panels) {

            //     var ref = panels[i].attr("data-section-name");

            //     $(".pagination .active").removeClass("active");

            //     $(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
            // },
            // afterRender:function() {
            //     var pagination = "<ul class=\"pagination\">";
            //     var activeClass = "";
            //     $("section").each(function(i) {
            //         activeClass = "";
            //         if(i===0) {
            //             activeClass = "active";
            //         }
            //         pagination += "<li><a class=\"" + activeClass + "\" href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-name").charAt(0).toUpperCase() + $(this).attr("data-section-name").slice(1) + "</span></a></li>";
            //     });

            //     pagination += "</ul>";

            //     $(".sidebar").append(pagination);
            //     /*

            //     Tip: The two click events below are the same:

            //     $(".pagination a").on("click",function() {
            //         $.scrollify.move($(this).attr("href"));
            //     });

            //     */
            //     $(".pagination a").on("click",$.scrollify.move);
            // }
        });
    }

}(jQuery));