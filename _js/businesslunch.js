;(function () {

    /*===================================
    =            Scrollmagic            =
    ===================================*/

    var sectionWeekMenuTitle = $('#sectionWeekMenu .category-section__title > h2'),
        sectionTopHeroTitle = $('#sectionTopHero .category-section__title > h2'),
        sectionTopHeroImage = $('#sectionTopHero .top-hero__image, #sectionTopHero .top-hero__pattern, #sectionTopHero .top-hero__overlay, #sectionTopHero .arrow-down'),
        sectionTopHeroArrow = $('.arrow-down'),
        sectionTopHeroText = $('.top-hero__text')
    ;

    var controller = new ScrollMagic.Controller({
        // refreshInterval: 300
    });

    /*----------  WeekMenu Section  ----------*/
    function sectionWeekMenuScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionTopHeroTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .to(sectionTopHeroImage, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .to(sectionTopHeroArrow, 0.3, {
                yPercent: "-100",
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            }, "-=1.2")
            .to(sectionTopHeroText, 0.3, {
                // yPercent: "-50",
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            }, "-=1")
            // .fromTo(sectionWeekMenuTitle, 0.3, {
            //     // yPercent: sectionPercent,
            //     autoAlpha: 0
            // }, {
            //     // yPercent: 0,
            //     autoAlpha: 1,
            //     ease: Power1.easeInOut
            // })
        ;

        var sectionWeekMenuAction = new ScrollMagic.Scene({
                triggerElement: sectionWeekMenu,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.top-hero__image', 'is-hidden')
            .setTween(tl)
            // .addIndicators({
            //     name: 'SanvichSection'
            // })
            .addTo(controller)
        ;
    }

    /*=====  End of Scrollmagic  ======*/

    /*=================================
    =            Week Tabs            =
    =================================*/



    /*=====  End of Week Tabs  ======*/

    sectionWeekMenuScroll();

}(jQuery));