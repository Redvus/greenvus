;(function () {

    // var Scrollbar = window.Scrollbar;
    // Scrollbar.init(document.getElementById('main-scrollbar'), {
    //     speed: 1,
    //     overscrollEffectColor: ''
    // });

    /*===================================
    =            Scrollmagic            =
    ===================================*/

    var sectionBurger = $('#sectionBurger')[0],
        sectionSandvich = $('#sectionSandvich')[0],
        sectionHotdog = $('#sectionHotdog')[0],
        sectionDrinks = $('#sectionDrinks')[0],
        sectionSalads = $('#sectionSalads')[0],
        sectionBurgerImage = $('#sectionBurger .category-section__image > img'),
        sectionSandvichImage = $('#sectionSandvich .category-section__image > img'),
        sectionHotdogImage = $('#sectionHotdog .category-section__image > img'),
        sectionDrinksImage = $('#sectionDrinks .category-section__image > img'),
        sectionSaladsImage = $('#sectionSalads .category-section__image > img'),
        sectionBurgerTitle = $('#sectionBurger .category-section__title > h2'),
        sectionSandvichTitle = $('#sectionSandvich .category-section__title > h2'),
        sectionHotdogTitle = $('#sectionHotdog .category-section__title > h2'),
        sectionDrinksTitle = $('#sectionDrinks .category-section__title > h2'),
        sectionSaladsTitle = $('#sectionSalads .category-section__title > h2'),
        sectionBurgerContent = $('#sectionBurger .category-section__content'),
        sectionSandvichContent = $('#sectionSandvich .category-section__content'),
        sectionHotdogContent = $('#sectionHotdog .category-section__content'),
        sectionDrinksContent = $('#sectionDrinks .category-section__content'),
        sectionSaladsContent = $('#sectionSalads .category-section__content'),
        sectionPercent = 10,
        sectionTopHeroTitle = $('#sectionTopHero .category-section__title > h2'),
        sectionTopHeroImage = $('#sectionTopHero .top-hero__image, #sectionTopHero .top-hero__pattern, #sectionTopHero .top-hero__overlay, #sectionTopHero .arrow-down'),
        sectionTopHeroArrow = $('.arrow-down'),
        sectionTopHeroText = $('.top-hero__text')
    ;

    var controller = new ScrollMagic.Controller({
        // refreshInterval: 300
    });

    /*----------  Burger Section  ----------*/
    function sectionBurgerScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionTopHeroTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .to(sectionTopHeroImage, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .to(sectionTopHeroArrow, 0.3, {
                yPercent: "-100",
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            }, "-=1.2")
            .to(sectionTopHeroText, 0.3, {
                // yPercent: "-50",
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            }, "-=1")
            // .fromTo(sectionBurgerTitle, 0.3, {
            //     // yPercent: sectionPercent,
            //     autoAlpha: 0
            // }, {
            //     // yPercent: 0,
            //     autoAlpha: 1,
            //     ease: Power1.easeInOut
            // })
        ;

        var sectionBurgerAction = new ScrollMagic.Scene({
                triggerElement: sectionBurger,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.top-hero__image', 'is-hidden')
            .setTween(tl)
            // .addIndicators({
            //     name: 'SanvichSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Sandvich Section  ----------*/
    function sectionSandvichScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionBurgerTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo(sectionSandvichTitle, 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                // yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectionSandvichAction = new ScrollMagic.Scene({
                triggerElement: sectionSandvich,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'SanvichSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Hotdog Section  ----------*/
    function sectionHotdogScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionSandvichTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo(sectionHotdogTitle, 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                // yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectioHotdogAction = new ScrollMagic.Scene({
                triggerElement: sectionHotdog,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'HotdogSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Salads Section  ----------*/
    function sectionSaladsScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionHotdogTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo(sectionSaladsTitle, 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                // yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectioSaladsAction = new ScrollMagic.Scene({
                triggerElement: sectionSalads,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'HotdogSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Drinks Section  ----------*/
    function sectionDrinksScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionSaladsTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo(sectionDrinksTitle, 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                // yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectioDrinksAction = new ScrollMagic.Scene({
                triggerElement: sectionDrinks,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'HotdogSection'
            // })
            .addTo(controller)
        ;
    }

    function initFstfood() {
        sectionBurgerScroll();
        // sectionSandvichScroll();
        // sectionHotdogScroll();
        // sectionDrinksScroll();
        // sectionSaladsScroll();
    }

    initFstfood();

    /*=====  End of Scrollmagic  ======*/

    /*=============================
    =            Thumb            =
    =============================*/

    var thumbLarge = $(".product-category__image--large"),
        thumbXXLarge = $(".product-category__image--xxlarge"),
        productCategoryParent = $(".product-category")
    ;

    setTimeout(function() {
        if($(window).width() > 1680 || screen.width > 1680) {
            productCategoryParent.is(thumbLarge);
            productCategoryParent.is(thumbXXLarge);
            thumbLarge.remove();
            thumbXXLarge.add();
        } else {
            productCategoryParent.is(thumbLarge);
            productCategoryParent.is(thumbXXLarge);
            thumbXXLarge.remove();
            thumbLarge.add();
        }
    }, 1);

    /*=====  End of Thumb  ======*/


}(jQuery));