;(function () {

    /*==================================
    =            Cookie 18+            =
    ==================================*/

    var permissionBlock = $('#permissionBlock'),
        permissionWindow =$('#permissionWindow'),
        permissionNope = $('#permissionNope'),
        permissionText = $('.permission-content__welcome'),
        permissionYes = $('#permissionYes'),
        permissionAlert = $('#permissionAlert'),
        permissionAway = $('#permissionAway')
    ;

    function cookieFront() {

        if (!$.cookie('was')) {

            permissionBlock.removeClass('permission-hidden');

        }

        $.cookie('was', true, {
            expires: 7,
            path: '/'
        });

    }

    function permissioEnter() {

        var tl = new TimelineMax();

        tl
            .to(permissionWindow, 1.2, {
                y: '-100%',
                autoAlpha: 0,
                ease: Back.easeInOut
            })
            .to(permissionBlock, 0.6, {
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .set(permissionBlock, {
                className: "+=permission-hidden"
            })
        ;

    }

    function permissionFront() {

        permissionYes.on('click', function () {
            permissioEnter();
        });

        function permissioAlert() {

            var tl = new TimelineMax({reversed:true, paused:true});

            tl
                .to(permissionYes, 0.4, {
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                })
                .to(permissionNope, 0.4, {
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                }, '-=0.4')
                .to(permissionText, 0.4, {
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                }, '-=0.4')
                .to(permissionAlert, 0.4, {
                    display: "flex",
                    autoAlpha: 1,
                    ease: Power1.easeInOut
                })
                .to(permissionAway, 0.4, {
                    display: "flex",
                    autoAlpha: 1,
                    ease: Power1.easeInOut
                })
            ;

            permissionNope.click(function () {
                tl.reversed() ? tl.restart() : tl.reverse();
            });
            permissionAway.on('click', function () {
                permissioEnter();
            });
            return tl;

        }
        permissioAlert();

    }
    // permissionFront();
    // cookieFront();

    /*=====  End of Cookie 18+  ======*/

    /*=============================================
    =            Front Video Backgound            =
    =============================================*/

    // var slider = new MasterSlider();

    // slider.setup('masterslider' , {
    //     width: 1280,
    //     height: 720,
    //     autoHeight: true,
    //     loop: true,
    //     autoplay: true,
    //     speed: 2,
    //     instantStartLayers: true,
    //     view: 'fade',
    //     layout: 'fullscreen',
    //     grabCursor: false,
    //     swipe: false,
    //     mouse: false
    // });

    /*=====  End of Front Video Backgound  ======*/

    /*================================
    =            MiniCart            =
    ================================*/

    var miniCartToggle = $('#minicartToggle'),
        miniCartContent = $('#miniCartContent'),
        miniCartClose = $('#minicartClose'),
        miniCartBlock = $('.header__cart'),
        // minicartFull = $('#minicartFull'),
        minicartWrapper = $('.wrapper')
    ;

    // TweenMax.set(miniCartContent, {
    //     right: "-100%",
    //     display: "none"
    // });

    function miniCartRight() {

        var tl = new TimelineMax({reversed:true});

        tl
            .to(miniCartContent, 0.6, {
                display: "flex",
                right: 0,
                zIndex: 9999,
                ease: Power1.easeInOut
            }, "-=0.6")
            .to(miniCartBlock, 0.6, {
                autoAlpha: 0,
                ease: Power1.easeInOut
            }, "-=0.6")
            .from(miniCartClose, 0.6, {
                autoAlpha: 0
            })
        ;

        miniCartToggle.on("click", function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        minicartWrapper.on("click", function () {
            tl.reverse();
        });
        miniCartClose.on("click", function () {
            tl.reverse();
        });
        $(window).bind('keydown', function(e) {
            if( e.keyCode == 27 ) {
                tl.reverse();
            }
        });

        return tl;

    }

    // miniCartRight();

    /*=====  End of MiniCart  ======*/

    var pagination = "<ul class=\"pagination\">",
        // activeClass = "",
        titleFastfoodSection = $('#sectionFastfood h2'),
        titleHolidaysSection = $('#sectionHolidays h2')
    ;

    //Scrollify
    // if($(window).width() > 570 || screen.width > 570) {
    //     //Scrollify. Пролистывание страниц с фиксацией и анимацией
    //     $.scrollify({
    //         section : ".section",
    //         // interstitialSection : ".footer",
    //         // easing: "easeOutExpo",
    //         // updateHash: false,
    //         scrollbars: true,
    //         // before:function(i,panels) {

    //         //     var ref = panels[i].attr("data-section-name");

    //         //     $(".pagination .active").removeClass("active");

    //         //     $(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
    //         // },
    //         // before: function(i,panels) {
    //         //     var ref = panels[i].attr("data-section-name");

    //         //     if(ref==="Fastfood") {
    //         //         TweenMax.to(titleFastfoodSection, 1, {
    //         //             yPercent: "0",
    //         //             autoAlpha: 1,
    //         //             // ease: Power2.easeInOut
    //         //         });
    //         //     }
    //         //     if(ref==="Holidays") {
    //         //         TweenMax.to(titleFastfoodSection, 1, {
    //         //             yPercent: "-50",
    //         //             autoAlpha: 0,
    //         //             // ease: Power2.easeInOut
    //         //         });
    //         //     }


    //         // },
    //         after:function() {

    //             // titleFrontSection.addClass('is-active');
    //             // TweenMax.from(titleFrontSection, 1, {
    //             //     yPercent: "-15%",
    //             //     autoAlpha: 1,
    //             //     ease: Power3.easeInOut
    //             // });

    //             // $(".section").each(function(i) {
    //             //     // activeClass = "";
    //             //     // if(i===0) {
    //             //     //     activeClass = "active";
    //             //     // }

    //             //     // pagination += "<li><a class=\"" + activeClass + "\" href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-name").charAt(0).toUpperCase() + $(this).attr("data-section-name").slice(1) + "</span></a></li>";

    //             // });

    //             // pagination += "</ul>";

    //             // $("#wrapperCategory").append(pagination);
    //             /*

    //             Tip: The two click events below are the same:

    //             $(".pagination a").on("click",function() {
    //                 $.scrollify.move($(this).attr("href"));
    //             });

    //             */
    //             // $(".pagination a").on("click",$.scrollify.move);
    //         }
    //     });
    // }

    var sectionFrontFirst = $('#sectionFrontFirst'),
        sectionFrontSecond = $('#sectionFrontSecond'),
        sectionFrontThird = $('#sectionFrontThird'),
        // sectionEach = document.getElementsByClassName('section a')
        sectionEach = $('.section'),
        sectionSpan = $('.section a > span'),
        sectionFrontFirstImg = $('#sectionFrontFirst .section__image img'),
        sectionFrontSecondImg = $('#sectionFrontSecond .section__image img'),
        sectionFrontThirdImg = $('#sectionFrontThird .section__image img')
    ;

    if($(window).width() > 570 || screen.width > 570) {

        // console.log(sectionEach.length);

        // for (var i = 0; i < sectionEach.length; i++) {
        //     console.log(sectionEach.length);
        //     // sectionEach[i].onmouseenter(function() {
        //     //     // $('.section__pattern').addClass('is-opacity');
        //     //     $('.section__image img').addClass('is-blur');
        //     // }).onmouseleave(function() {
        //     //     // $('.section__pattern').removeClass('is-opacity');
        //     //     $('.section__image img').removeClass('is-blur');
        //     // });
        // }

        // $('.section').each(function() {
        //     $(this).find('a').mouseover(function() {
        //         // $('.section__pattern').addClass('is-opacity');
        //         $('.section__image img').addClass('is-blur');
        //     }).mouseleave(function() {
        //         // $('.section__pattern').removeClass('is-opacity');
        //         $('.section__image img').removeClass('is-blur');
        //     });
        // });

        sectionFrontFirst.mouseover(function() {
            // $('#sectionFastfood .section__pattern').addClass('is-opacity');
            sectionFrontFirstImg.addClass('is-blur');
            // $('#sectionFastfood a > span').addClass('is-position');
        }).mouseleave(function() {
            // $('#sectionFastfood .section__pattern').removeClass('is-opacity');
            sectionFrontFirstImg.removeClass('is-blur');
            // $('#sectionFastfood a > span').removeClass('is-position');
        });

        sectionFrontSecond.mouseover(function() {
            // $('#sectionCatering .section__pattern').addClass('is-opacity');
            sectionFrontSecondImg.addClass('is-blur');
        }).mouseleave(function() {
            // $('#sectionCatering .section__pattern').removeClass('is-opacity');
            sectionFrontSecondImg.removeClass('is-blur');
        });

        sectionFrontThird.mouseover(function() {
            // $('#sectionBusinessLunch .section__pattern').addClass('is-opacity');
            sectionFrontThirdImg.addClass('is-blur');
        }).mouseleave(function() {
            // $('#sectionBusinessLunch .section__pattern').removeClass('is-opacity');
            sectionFrontThirdImg.removeClass('is-blur');
        });

    }

}(jQuery));