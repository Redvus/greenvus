;(function () {

    var navMenu = $(".header__nav"),
        headerLeft = $("#headerLeft"),
        headerMenu = $('#headerMenu'),
        headerLogo = $('.header__logo'),
        headerLogoSVG = $('.header__logo img'),
        headerIcon = $('.header__icon'),
        headerLink = $('#nav ul li'),
        navClose = $('#navClose'),
        headerBack = $('#headerBack'),
        footerMenu = $('.footer')
    ;

    var miniCartToggle = $('#minicartToggle'),
        miniCartContent = $('#miniCartContent'),
        miniCartClose = $('#minicartClose'),
        miniCartBlock = $('.header__cart'),
        // minicartFull = $('#minicartFull'),
        minicartWrapper = $('.wrapper'),
        miniCartToCart = $('#miniCartToCart'),
        minicartContent = $('#minicartContent')
    ;

    if($(window).width() > 570 || screen.width > 570) {

        function menuLeft() {

            var tl = new TimelineMax({reversed:true, paused:true});

            tl
                .to(headerLeft, 0.6, {
                    width: "20rem",
                    ease: Circ.easeInOut
                }, "-=0.6")
                .from(navMenu, 1, {
                   left: "-100%",
                   autoAlpha: 0,
                   ease: Circ.easeInOut
                }, "-=1")
                .to([headerIcon, footerMenu], 0.4, {
                    autoAlpha: 0,
                    ease: Circ.easeInOut
                }, "-=0.9")
                .from(headerBack, 0.8, {
                    autoAlpha: 0,
                    ease: Circ.easeInOut
                }, "-=0.8")
                .to(headerLogo, 0.6, {
                    height: "8rem",
                    ease: Circ.easeInOut
                }, "-=0.6")
                .to(headerLogoSVG, 0.6, {
                    height: "8rem",
                    ease: Circ.easeInOut
                }, "-=0.5")
                .staggerFrom(headerLink, 0.6, {
                    delay: 0.6,
                    y: "50%",
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                }, 0.1, "-=0.6")
                .from(navClose, 0.4, {
                   autoAlpha: 0,
                   zIndex: 9995,
                   ease: Power1.easeInOut
                }, "-=0.6")
            ;

            headerMenu.click(function () {
                tl.reversed() ? tl.restart() : tl.reverse();
            });
            navClose.click(function () {
                tl.reverse();
            });
            headerBack.click(function () {
                tl.reverse();
            });
            // $(window).bind('keydown', function(e){
            //     if (e.keyCode==27) {
            //         tl.reverse(-0.8);
            //     }
            // });

            return tl;
        }

        menuLeft();
    }

    if($(window).width() < 570 || screen.width < 570) {

        function menuLeftMobile() {

            var tl = new TimelineMax({reversed:true, paused:true});

            tl
                .to(headerLeft, 0.6, {
                    width: "100%",
                    ease: Circ.easeInOut
                }, "-=0.6")
                .to(minicartContent, 0.6, {
                    // autoAlpha: 0,
                    xPercent: 100,
                    ease: Circ.easeInOut
                }, "-=0.6")
                .from(navMenu, 1, {
                   left: "-100%",
                   autoAlpha: 0,
                   ease: Circ.easeInOut
                }, "-=0.9")
                .to(headerIcon, 0.4, {
                    autoAlpha: 0,
                    ease: Circ.easeInOut
                }, "-=0.9")
                .to(minicartContent, 0.4, {
                    // autoAlpha: 0,
                    xPercent: 100,
                    ease: Circ.easeInOut
                }, "-=0.9")
                // .from(headerBack, 0.8, {
                //     autoAlpha: 0,
                //     ease: Circ.easeInOut
                // }, "-=0.8")
                .staggerFrom(headerLink, 0.6, {
                    delay: 0.6,
                    y: "50%",
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                }, 0.1, "-=0.6")
                .from(navClose, 0.6, {
                   autoAlpha: 0,
                   // x: "100%",
                   ease: Power1.easeInOut
                }, "-=0.6")
            ;

            headerMenu.click(function () {
                tl.reversed() ? tl.restart() : tl.reverse();
            });
            navClose.click(function () {
                tl.reverse();
            });

            return tl;
        }

        menuLeftMobile();
    }

    /*================================
    =            MiniCart            =
    ================================*/

    // TweenMax.set(miniCartContent, {
    //     right: "-100%",
    //     display: "none"
    // });

    if($(window).width() < 570 || screen.width < 570) {

        function miniCartRight() {

            var tl = new TimelineMax({reversed:true, paused:true});

            tl
                .to(miniCartContent, 0.6, {
                    display: "flex",
                    right: 0,
                    zIndex: 9990,
                    ease: Power1.easeInOut
                }, "-=0.6")
                .to(headerIcon, 0.4, {
                    autoAlpha: 0,
                    ease: Circ.easeInOut
                }, "-=0.9")
                .from(miniCartToCart, 0.4, {
                    delay: 0.6,
                    top: '-3rem',
                    autoAlpha: 0,
                    ease: Circ.easeInOut
                }, "-=0.9")
                .to(miniCartBlock, 0.6, {
                    autoAlpha: 0,
                    ease: Power1.easeInOut
                }, "-=0.6")
                .from(headerBack, 0.4, {
                    autoAlpha: 0,
                    ease: Circ.easeInOut
                }, "-=0.4")
                .from(miniCartClose, 0.6, {
                    autoAlpha: 0
                })
            ;

            miniCartToggle.on("click", function () {
                tl.reversed() ? tl.restart() : tl.reverse(-0.4);
            });
            headerBack.on("click", function () {
                tl.reverse(-0.4);
            });
            miniCartClose.on("click", function () {
                tl.reverse();
            });

            return tl;

        }

        miniCartRight();

    } else {
        $('#minicartToggle').on("click", function() {
            location.href = "/cart";
        });
    }

    /*=====  End of MiniCart  ======*/


}(jQuery));