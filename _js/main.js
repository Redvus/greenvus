;(function (e) {

    /*===================================
    =            Hero Slider            =
    ===================================*/

    // function HeroSlider( element ) {
    //     this.element = element;
    //     this.navigation = this.element.getElementsByClassName("section-slider__nav")[0];
    //     this.navigationItems = this.navigation.getElementsByTagName('li');
    //     // this.marker = this.navigation.getElementsByClassName("section-slider__marker")[0];
    //     this.slides = this.element.getElementsByClassName("section-slider__slide");
    //     this.slidesNumber = this.slides.length;
    //     this.newSlideIndex = 0;
    //     this.oldSlideIndex = 0;
    //     this.init();
    // };

    // HeroSlider.prototype.init = function() {
    //     var self = this;
    //     this.navigation.addEventListener('click', function(event){
    //         if( event.target.tagName.toLowerCase() == 'div' )
    //             return;
    //         event.preventDefault();
    //         var selectedSlide = event.target;
    //         if( hasClass(event.target.parentElement, 'cd-selected') )
    //             return;
    //         self.oldSlideIndex = self.newSlideIndex;
    //         self.newSlideIndex = Array.prototype.indexOf.call(self.navigationItems, event.target.parentElement);
    //         self.newSlide();
    //         // self.updateNavigationMarker();
    //         self.updateSliderNavigation();
    //     });
    // };

    // HeroSlider.prototype.newSlide = function(direction) {
    //     var self = this,
    //         $sliderImage = $('.section-slider__image > img'),
    //         $sliderProduct = $('.product-category'),
    //         $sliderProductTitle = $('.product-category__title > h2'),
    //         $sliderProductPrice = $('.product-category__price > .price > h3'),
    //         $sliderProductText = $('.product-category__text > p')
    //     ;
    //     removeClass(this.slides[this.oldSlideIndex], "section-slider__slide--selected section-slider__slide--from-left section-slider__slide--from-right");
    //     // addClass(this.slides[this.oldSlideIndex], "section-slider__slide--is-moving");
    //     // setTimeout(function(){removeClass(self.slides[self.oldSlideIndex], "section-slider__slide--is-moving");}, 500);
    //     TweenMax.staggerFrom($sliderProduct, 0.6, {
    //         y: "15%",
    //         autoAlpha: 0,
    //         ease: Back.easeInOut
    //     }, 0.05);
    //     TweenMax.from($sliderImage, 0.8, {
    //         y: "-55%",
    //         autoAlpha: 0,
    //         ease: Back.easeInOut
    //     });
    //     // TweenMax.from($sliderProductTitle, 0.8, {
    //     //     y: "15%",
    //     //     autoAlpha: 0,
    //     //     ease: Back.easeInOut
    //     // });
    //     // TweenMax.from($sliderProductPrice, 0.8, {
    //     //     x: "15%",
    //     //     autoAlpha: 0,
    //     //     delay: 0.5,
    //     //     ease: Back.easeInOut
    //     // });
    //     // TweenMax.from($sliderProductText, 0.8, {
    //     //     y: "-30%",
    //     //     autoAlpha: 0,
    //     //     ease: Back.easeInOut
    //     // });

    //     for(var i=0; i < this.slidesNumber; i++) {
    //         if( i < this.newSlideIndex && this.oldSlideIndex < this.newSlideIndex) {
    //             addClass(this.slides[i], "section-slider__slide--move-left");
    //         } else if( i == this.newSlideIndex && this.oldSlideIndex < this.newSlideIndex) {
    //             addClass(this.slides[i], "section-slider__slide--selected section-slider__slide--from-right");
    //         } else if(i == this.newSlideIndex && this.oldSlideIndex > this.newSlideIndex) {
    //             addClass(this.slides[i], "section-slider__slide--selected section-slider__slide--from-left");
    //             removeClass(this.slides[i], "section-slider__slide--move-left");
    //         } else if( i > this.newSlideIndex && this.oldSlideIndex > this.newSlideIndex ) {
    //             removeClass(this.slides[i], "section-slider__slide--move-left");
    //         }
    //     }

    // };

    // HeroSlider.prototype.updateNavigationMarker = function() {
    //     removeClassPrefix(this.marker, 'item');
    //     addClass(this.marker, "section-slider__marker--item-"+ (Number(this.newSlideIndex) + 1));
    // };

    // HeroSlider.prototype.updateSliderNavigation = function() {
    //     removeClass(this.navigationItems[this.oldSlideIndex], 'cd-selected');
    //     addClass(this.navigationItems[this.newSlideIndex], 'cd-selected');
    // };

    // var heroSliders = document.getElementsByClassName("section-slider");
    // if( heroSliders.length > 0 ) {
    //     for( var i = 0; i < heroSliders.length; i++) {
    //         (function(i){
    //             new HeroSlider(heroSliders[i])
    //         })(i);
    //     }
    // }

    //on mobile - open/close primary navigation clicking/tapping the menu icon
    // document.getElementsByClassName('js-cd-header__nav')[0].addEventListener('click', function(event){
    //     if(event.target.tagName.toLowerCase() == 'nav') {
    //         classie.toggleClass(this.getElementsByTagName('ul')[0], 'cd-is-visible');
    //     }
    // });

    // function removeClassPrefix(el, prefix) {
    //     //remove all classes starting with 'prefix'
    //     var classes = el.className.split(" ").filter(function(c) {
    //         return c.indexOf(prefix) < 0;
    //     });
    //     el.className = classes.join(" ");
    // };

    //class manipulations - needed if classList is not supported
    // function hasClass(el, className) {
    //     if (el.classList) return el.classList.contains(className);
    //     else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    // }
    // function addClass(el, className) {
    //     var classList = className.split(' ');
    //     if (el.classList) el.classList.add(classList[0]);
    //     else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
    //     if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
    // }
    // function removeClass(el, className) {
    //     var classList = className.split(' ');
    //     if (el.classList) el.classList.remove(classList[0]);
    //     else if(hasClass(el, classList[0])) {
    //         var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
    //         el.className=el.className.replace(reg, ' ');
    //     }
    //     if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
    // }

    /*=====  End of Hero Slider  ======*/

    // $(document).ready(function() {
    //     function n() {
    //         if ("delivery_1" != $('[name="delivery"]:checked').attr("id")) return $("#address").show(), !1;
    //         $("#address").hide();
    //     }
    // });

    /*================================
    =            Delivery            =
    ================================*/

    function hideaddress() {
        if ($('[name="delivery"]:checked').attr('id') == 'delivery_1') {
            $("#address").hide();
        } else {
            $("#address").show();
            return false;
        }
    }

    window.onload = function () {
       hideaddress();
    };

    $(document).on('change', '[name="delivery"]', function() {
       hideaddress();
    });

    /*=====  End of Delivery  ======*/

    /*=============================
    =            Input            =
    =============================*/

    $('.form-group__minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.form-group__plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    // $('.cart-order__client [name=phone]').inputmask("+7 (999) 999 99 99");
    // $('.cart-order__client [name=email]').inputmask("*{1,64}@*{1,64}[.a{1,3}]");
    // $('[name=receiver]').inputmask("a{4,64}");

    /*=====  End of Input  ======*/



}(jQuery));