;(function () {

    /*===================================
    =            Scrollmagic            =
    ===================================*/

    var sectionHotImage = $('#sectionHot .category-section__image > img'),
        sectionHotTitle = $('#sectionHot .category-section__title > h2'),
        sectionHotConent = $('#sectionHot .category-section__content'),
        sectionGarnishImage = $('#sectionGarnish .category-section__image > img'),
        sectionGarnishTitle = $('#sectionGarnish .category-section__title > h2'),
        sectionGarnishConent = $('#sectionGarnish .category-section__content'),
        sectionHotAppertizersImage = $('#sectionHotAppertizers .category-section__image > img'),
        sectionHotAppertizersTitle = $('#sectionHotAppertizers .category-section__title > h2'),
        sectionHotAppertizersConent = $('#sectionHotAppertizers .category-section__content'),
        sectionColdAppertizersImage = $('#sectionColdAppertizers .category-section__image > img'),
        sectionColdAppertizersTitle = $('#sectionColdAppertizers .category-section__title > h2'),
        sectionColdAppertizersConent = $('#sectionColdAppertizers .category-section__content'),
        sectionPercent = 10
    ;

    var controller = new ScrollMagic.Controller({
        // refreshInterval: 300
    });

    /*----------  Garnish Section  ----------*/
    function sectionGarnishScroll() {
        var tl = new TimelineMax();
        tl
            .to([sectionHotImage, sectionHotTitle], 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo([sectionGarnishImage, sectionGarnishTitle], 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectionGarnishAction = new ScrollMagic.Scene({
                triggerElement: sectionGarnish,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'BurgerSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  HotAppertizers Section  ----------*/
    function sectionHotAppertizersScroll() {
        var tl = new TimelineMax();
        tl
            .to([sectionGarnishImage, sectionGarnishTitle], 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo([sectionHotAppertizersImage, sectionHotAppertizersTitle], 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectionHotAppertizersAction = new ScrollMagic.Scene({
                triggerElement: sectionHotAppertizers,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'BurgerSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  ColdAppertizers Section  ----------*/
    function sectionColdAppertizersScroll() {
        var tl = new TimelineMax();
        tl
            .to([sectionHotAppertizersImage, sectionHotAppertizersTitle], 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0,
                // display: 'n_25',
                ease: Power1.easeInOut
            })
            .fromTo([sectionColdAppertizersImage, sectionColdAppertizersTitle], 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectionColdAppertizersAction = new ScrollMagic.Scene({
                triggerElement: sectionColdAppertizers,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'BurgerSection'
            // })
            .addTo(controller)
        ;
    }

    sectionGarnishScroll();
    sectionHotAppertizersScroll();
    sectionColdAppertizersScroll();

    /*=====  End of Scrollmagic  ======*/

}(jQuery));