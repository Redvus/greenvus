;(function () {

    /*===================================
    =            Scrollmagic            =
    ===================================*/

    var sectionAgreement = $('#sectionAgreement')[0],
        sectionShipping = $('#sectionShipping')[0],
        sectionOferta = $('#sectionOferta')[0],
        sectionPayment = $('#sectionPayment')[0],
        sectionAgreementTitle = $('#sectionAgreement .category-section__title > h2'),
        sectionShippingTitle = $('#sectionShipping .category-section__title > h2'),
        sectionOfertaTitle = $('#sectionOferta .category-section__title > h2'),
        sectionPaymentTitle = $('#sectionPayment .category-section__title > h2')
    ;

    var controller = new ScrollMagic.Controller({
        // refreshInterval: 300
    });

    /*----------  Oferta Section  ----------*/
    function sectionOfertaScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionAgreementTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0
                // display: 'n_25',
                // ease: Power1.easeInOut
            })
            .fromTo(sectionOfertaTitle, 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                // yPercent: 0,
                autoAlpha: 1,
                ease: Power1.easeInOut
            })
        ;

        var sectionOfertaAction = new ScrollMagic.Scene({
                triggerElement: sectionOferta,
                triggerHook: 1,
                duration: '100%'
            })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'SanvichSection'
            // })
            .addTo(controller)
        ;
    }

    /*----------  Payment Section  ----------*/
    function sectionPaymentScroll() {
        var tl = new TimelineMax();
        tl
            .to(sectionOfertaTitle, 0.3, {
                // yPercent: -sectionPercent,
                autoAlpha: 0
                // display: 'n_25',
                // ease: Power1.easeInOut
            })
            .fromTo(sectionPaymentTitle, 0.3, {
                // yPercent: sectionPercent,
                autoAlpha: 0
            }, {
                    // yPercent: 0,
                    autoAlpha: 1,
                    ease: Power1.easeInOut
                })
            ;

        var sectionPaymentAction = new ScrollMagic.Scene({
            triggerElement: sectionPayment,
            triggerHook: 1,
            duration: '100%'
        })
            // .setClassToggle('.category-section__image > img', 'is-active')
            .setTween(tl)
            // .addIndicators({
            //     name: 'SanvichSection'
            // })
            .addTo(controller)
        ;
    }

    // sectionOfertaScroll();
    // sectionPaymentScroll();

    /*=====  End of Scrollmagic  ======*/


}(jQuery));